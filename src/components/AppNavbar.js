import { Container, Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import { useState, Fragment, useContext, useEffect } from 'react';
import UserContext from '../UserContext';
import AppNavbarCSS from "../AppNavbar.css";

function AppNavbar() {
    const { user } = useContext(UserContext);

    const [navbarBrandText, setNavbarBrandText] = useState("MORANTE CAPSTON3 STORE");

    useEffect(() => {
    const navbarBrand = document.querySelector(".navbar-brand");

    let interval = null;

    navbarBrand.onmouseover = event => {
    let iteration = 0;

    clearInterval(interval);

    interval = setInterval(() => {
        setNavbarBrandText(prevText =>
    prevText.split("")
            .map((letter, index) => {
            if (index < iteration) {
                return event.target.dataset.value[index];
                }
              return allLetters[Math.floor(Math.random() * 26)];
            })
            .join("")
        );

        if (iteration >= event.target.dataset.value.length) {
        clearInterval(interval);
        }

        iteration += 1 / 3;
        }, 30);
    };

    return () => {
    clearInterval(interval);
    };
    }, []);

    const allLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    return (
    <Navbar expand="lg">
    <Container fluid>
        <Navbar.Brand data-value="MORANTE CAPSTON3 STORE" as={Link} to="/">{navbarBrandText}</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="m-auto">
            <Nav.Link as={NavLink} activeClassName="active" to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} activeClassName="active" to="/products">Shop Products</Nav.Link>
            {user.id !== null ? (
            <Nav.Link as={NavLink} activeClassName="active" to="/logout">Logout</Nav.Link>)
            :
            (
            <Fragment>
                <Nav.Link as={NavLink} activeClassName="active" to="/login">Login</Nav.Link>
                <Nav.Link as={NavLink} activeClassName="active" to="/register">Register</Nav.Link>
            </Fragment>
            )}
            {user.isAdmin && (
            <Nav.Link as={NavLink} activeClassName="active" to="/admindashboard">Admin Dashboard</Nav.Link>
            )}
        </Nav>
        </Navbar.Collapse>
        </Container>
    </Navbar>
    );
}

export default AppNavbar;
