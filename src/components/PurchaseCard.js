import { useState } from "react";
import { Modal, Button } from "react-bootstrap";

export default function QuantityModal(props) {

    const [quantity, setQuantity] = useState(1);

    const increaseQuantity = () => {
        setQuantity(quantity + 1);
    };

    const decreaseQuantity = () => {
        if (quantity > 1) {
            setQuantity(quantity - 1);
        }
    };

    const purchase = () => {
        props.onHide();
        props.purchase(props.productId);
    };

    return (
        <Modal {...props} centered>
            <Modal.Header closeButton>
                <Modal.Title>{props.productName}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>Price: PHP {props.price}</p>
                <p>Quantity:</p>
                <div className="d-flex align-items-center justify-content-between">
                    <Button variant="outline-secondary" size="sm" onClick={decreaseQuantity}>-</Button>
                    <span>{quantity}</span>
                    <Button variant="outline-secondary" size="sm" onClick={increaseQuantity}>+</Button>
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={props.onHide}>Cancel</Button>
                <Button variant="primary" onClick={purchase}>Purchase</Button>
            </Modal.Footer>
        </Modal>
    );

}
