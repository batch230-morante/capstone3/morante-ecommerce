import { useEffect, useState, useContext } from "react";

import { Navigate } from "react-router-dom";

import ProductCard from "../components/ProductCard";
import UserContext from "../UserContext";


export default function Products() {

	const { user } = useContext(UserContext);
	console.log(user);
	// couses state that will be used to store the courses retrieve in the database.

	const [products, setProducts] = useState([]);

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts/`, {
            method: "GET"
        })
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product =>{
				return(
					<ProductCard key={product._id} productProp={product}/>
				);
			}));
		})
	}, []);


	return(
		(user.isAdmin == true)
		?
			<Navigate to="/admindashboard" />
		:	
		<>
			<div className ="text-center" style={{ color: 'white', paddingTop: '20px'}}>
			<h1>Products Available:</h1>
			</div>
			{products}
		</>
	)
}